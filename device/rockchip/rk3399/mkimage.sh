#!/bin/bash
set -e

# ----------------------------------------------------------
. build/envsetup.sh >/dev/null

export PATH=$ANDROID_BUILD_PATHS:$PATH

TARGET_PRODUCT=$(get_build_var TARGET_PRODUCT)
TARGET_DEVICE_DIR=$(get_build_var TARGET_DEVICE_DIR)
BOARD_AVB_ENABLE=$(get_build_var BOARD_AVB_ENABLE)
BOARD_INCLUDE_DTB_IN_BOOTIMG=$(get_build_var BOARD_INCLUDE_DTB_IN_BOOTIMG)
PRODUCT_USE_DYNAMIC_PARTITIONS=$(get_build_var PRODUCT_USE_DYNAMIC_PARTITIONS)
HIGH_RELIABLE_RECOVERY_OTA=$(get_build_var HIGH_RELIABLE_RECOVERY_OTA)
BOARD_USES_AB_IMAGE=$(get_build_var BOARD_USES_AB_IMAGE)

echo TARGET_PRODUCT=$TARGET_PRODUCT
echo BOARD_AVB_ENABLE=$BOARD_AVB_ENABLE
echo BOARD_USES_AB_IMAGE=$BOARD_USES_AB_IMAGE
echo HIGH_RELIABLE_RECOVERY_OTA=$HIGH_RELIABLE_RECOVERY_OTA
echo ----------------------------------

IMAGE_PATH=rockdev/Image-$TARGET_PRODUCT
UBOOT_PATH=u-boot
KERNEL_PATH=kernel

if [ ! -z "$BOARD_INCLUDE_DTB_IN_BOOTIMG" ]; then
	KERNEL_DEBUG=$(get_build_var TARGET_PREBUILT_KERNEL)
	BOARD_BOOTIMG_HEADER_VERSION=$(get_build_var BOARD_BOOTIMG_HEADER_VERSION)
	PLATFORM_VERSION=$(get_build_var PLATFORM_VERSION)
	PLATFORM_SECURITY_PATCH=$(get_build_var PLATFORM_SECURITY_PATCH)
fi

FLASH_CONFIG_FILE=${TARGET_DEVICE_DIR}/config.cfg

BOOT_OTA="ota"
TARGET="withoutkernel"
[ -z "$1" ] || TARGET=$1
[ $TARGET != $BOOT_OTA -a $TARGET != "withoutkernel" ] && echo "unknow target[${TARGET}],exit!" && exit 0

[ $(id -u) -eq 0 ] || FAKEROOT=fakeroot

# ----------------------------------------------------------
# local functions

function make_dtbo_img() {
	echo -n "create dtbo.img...."
	if [ ! -f "$OUT/dtbo.img" ]; then
		BOARD_DTBO_IMG=$OUT/rebuild-dtbo.img
	else
		BOARD_DTBO_IMG=$OUT/dtbo.img
	fi
	cp -a $BOARD_DTBO_IMG $IMAGE_PATH/dtbo.img
	echo "done."
}

function make_boot_img() {
	echo "create boot.img...."
	if [ "$BOARD_AVB_ENABLE" = "true" ]; then
		cp -a $OUT/boot.img $IMAGE_PATH/boot.img
		cp -a $OUT/boot-debug.img $IMAGE_PATH/boot-debug.img
	elif [ -z "$BOARD_INCLUDE_DTB_IN_BOOTIMG" ]; then
		cp -a $OUT/boot.img $IMAGE_PATH/boot.img
	else
		echo "BOARD_AVB_ENABLE is false, make boot.img from kernel."
		local BOARD_KERNEL_CMDLINE=`get_build_var BOARD_KERNEL_CMDLINE`
		mkbootimg --kernel $KERNEL_DEBUG --ramdisk $OUT/ramdisk.img --second kernel/resource.img \
		  --header_version $BOARD_BOOTIMG_HEADER_VERSION \
		  --os_version $PLATFORM_VERSION --os_patch_level $PLATFORM_SECURITY_PATCH \
		  --cmdline "$BOARD_KERNEL_CMDLINE" --output $OUT/boot.img && \
			cp -a $OUT/boot.img $IMAGE_PATH/boot.img
	fi
	echo "done."
}

function make_recovery_img() {
	[ "$(get_build_var TARGET_NO_RECOVERY)" = "true" ] && return 0
	echo "create recovery.img...."
	if [ "$BOARD_AVB_ENABLE" = "true" ]; then
		cp -a $OUT/recovery.img $IMAGE_PATH/recovery.img
	elif [ -z "$BOARD_INCLUDE_DTB_IN_BOOTIMG" ]; then
		cp -a $OUT/recovery.img $IMAGE_PATH/recovery.img
	else
		echo "BOARD_AVB_ENABLE is false, make recovery.img from kernel && out."
		local ROCKCHIP_RECOVERYIMAGE_CMDLINE_ARGS=`get_build_var ROCKCHIP_RECOVERYIMAGE_CMDLINE_ARGS`
		[ -d $OUT/recovery/root ] && \
		mkbootfs -d $OUT/system $OUT/recovery/root | minigzip > $OUT/ramdisk-recovery.img && \
		mkbootimg --kernel $KERNEL_DEBUG --ramdisk $OUT/ramdisk-recovery.img --second kernel/resource.img \
		  --header_version $BOARD_BOOTIMG_HEADER_VERSION \
		  --os_version $PLATFORM_VERSION --os_patch_level $PLATFORM_SECURITY_PATCH \
		  --recovery_dtbo $BOARD_DTBO_IMG \
		  --cmdline "$ROCKCHIP_RECOVERYIMAGE_CMDLINE_ARGS" --output $OUT/recovery.img && \
			cp -a $OUT/recovery.img $IMAGE_PATH/recovery.img
	fi
	echo "done."
}

function make_system_img() {
	echo "create system.img...."
	if [ "$BOARD_AVB_ENABLE" = "true" ]; then
		echo "system.img has been signed by avbtool, just copy."
	else
		echo "BOARD_AVB_ENABLE is false, make system.img from out."
		[ -d $OUT/system ] && \
			python build/make/tools/releasetools/build_image.py \
			  $OUT/system \
			  $OUT/obj/PACKAGING/systemimage_intermediates/system_image_info.txt \
			  $OUT/system.img \
			  $OUT/system
	fi
	python device/rockchip/common/sparse_tool.py $OUT/system.img
	mv $OUT/system.img.out $OUT/system.img
	cp -f $OUT/system.img $IMAGE_PATH/system.img
	echo "done."
}

function make_vbmeta_img() {
	echo "create vbmeta.img...."
	if [ "$BOARD_AVB_ENABLE" = "true" ]; then
		cp -a $OUT/vbmeta.img $IMAGE_PATH/vbmeta.img
	else
		echo "BOARD_AVB_ENABLE is false, use default vbmeta.img"
		cp -a device/rockchip/common/vbmeta.img $IMAGE_PATH/vbmeta.img
	fi
	echo "done."
}

function make_vendor_img() {
	echo "create vendor.img..."
	if [ "$BOARD_AVB_ENABLE" = "true" ]; then
		echo "vendor.img has been signed by avbtool, just copy."
	else
		echo "BOARD_AVB_ENABLE is false, make vendor.img from out."
		[ -d $OUT/vendor ] && \
			python build/make/tools/releasetools/build_image.py \
			  $OUT/vendor \
			  $OUT/obj/PACKAGING/vendor_intermediates/vendor_image_info.txt \
			  $OUT/vendor.img \
			  $OUT/system
	fi
	python device/rockchip/common/sparse_tool.py $OUT/vendor.img
	mv $OUT/vendor.img.out $OUT/vendor.img
	cp -a $OUT/vendor.img $IMAGE_PATH/vendor.img
	echo "done."
}

function make_odm_img() {
	echo "create odm.img..."
	if [ "$BOARD_AVB_ENABLE" = "true" ]; then
		echo "odm.img has been signed by avbtool, just copy."
	else
		echo "BOARD_AVB_ENABLE is false, make odm.img from out."
		[ -d $OUT/odm ] && \
			python build/make/tools/releasetools/build_image.py \
			  $OUT/odm \
			  $OUT/obj/PACKAGING/odm_intermediates/odm_image_info.txt \
			  $OUT/odm.img \
			  $OUT/system
	fi
	python device/rockchip/common/sparse_tool.py $OUT/odm.img
	mv $OUT/odm.img.out $OUT/odm.img
	cp -f $OUT/odm.img $IMAGE_PATH/odm.img
	echo "done."
}

function copy_super_img() {
	[ "$PRODUCT_USE_DYNAMIC_PARTITIONS" = "true" ] || return 0
	echo -n "copy super.img..."
	cp -a $OUT/super.img $IMAGE_PATH/super.img
	echo "done."
}

function copy_misc_img() {
	echo -n "create misc.img...."
	cp -a rkst/Image/misc.img $IMAGE_PATH/misc.img
	cp -a rkst/Image/pcba_small_misc.img $IMAGE_PATH/pcba_small_misc.img
	cp -a rkst/Image/pcba_whole_misc.img $IMAGE_PATH/pcba_whole_misc.img
	echo "done."
}

function copy_uboot_img() {
	if [ -f $UBOOT_PATH/uboot.img ]; then
		echo -n "create uboot.img..."
		cp -a $UBOOT_PATH/uboot.img $IMAGE_PATH/uboot.img
		echo "done."
	else
		echo "$UBOOT_PATH/uboot.img not fount! Please make it from $UBOOT_PATH first!"
	fi
}

function copy_trust_img() {
	if [ -f $UBOOT_PATH/trust_nand.img ]; then
		echo -n "create trust.img..."
		cp -a $UBOOT_PATH/trust_nand.img $IMAGE_PATH/trust.img
		echo "done."
	elif [ -f $UBOOT_PATH/trust_with_ta.img ]; then
		echo -n "create trust.img..."
		cp -a $UBOOT_PATH/trust_with_ta.img $IMAGE_PATH/trust.img
		echo "done."
	elif [ -f $UBOOT_PATH/trust.img ]; then
		echo -n "create trust.img..."
		cp -a $UBOOT_PATH/trust.img $IMAGE_PATH/trust.img
		echo "done."
	else
		echo "$UBOOT_PATH/trust.img not fount! Please make it from $UBOOT_PATH first!"
	fi
}

function copy_uboot_ro_img() {
	[ "$HIGH_RELIABLE_RECOVERY_OTA" = "true" ] || return 0
	if [ -f $UBOOT_PATH/uboot_ro.img ]; then
		echo -n "HIGH_RELIABLE_RECOVERY_OTA is true. create uboot_ro.img..."
		cp -a $UBOOT_PATH/uboot_ro.img $IMAGE_PATH/uboot_ro.img
		cp -a $IMAGE_PATH/trust.img $IMAGE_PATH/trust_ro.img
		echo "done."
	else
		echo "$UBOOT_PATH/uboot_ro.img not fount! Please make it from $UBOOT_PATH first!"
	fi
}

function copy_rk_loader() {
	if [ -f $UBOOT_PATH/*_loader_*.bin ]; then
		echo -n "create loader..."
		cp -a $UBOOT_PATH/*_loader_*.bin $IMAGE_PATH/MiniLoaderAll.bin
		echo "done."
	else
		if [ -f $UBOOT_PATH/*loader*.bin ]; then
			echo -n "create loader..."
			cp -a $UBOOT_PATH/*loader*.bin $IMAGE_PATH/MiniLoaderAll.bin
			echo "done."
		elif [ "$TARGET_PRODUCT" == "px3" -a -f $UBOOT_PATH/RKPX3Loader_miniall.bin ]; then
			echo -n "create loader..."
			cp -a $UBOOT_PATH/RKPX3Loader_miniall.bin $IMAGE_PATH/MiniLoaderAll.bin
			echo "done."
		else
			echo "$UBOOT_PATH/*MiniLoaderAll_*.bin not fount! Please make it from $UBOOT_PATH first!"
		fi
	fi
}

function copy_resource_img() {
	if [ -f $KERNEL_PATH/resource.img ]; then
		echo -n "create resource.img..."
		cp -a $KERNEL_PATH/resource.img $IMAGE_PATH/resource.img
		echo "done."
	else
		echo "$KERNEL_PATH/resource.img not fount!"
	fi
}

function copy_kernel_img() {
	if [ -f $KERNEL_PATH/kernel.img ]; then
		echo -n "create kernel.img..."
		cp -a $KERNEL_PATH/kernel.img $IMAGE_PATH/kernel.img
		echo "done."
	else
		echo "$KERNEL_PATH/kernel.img not fount!"
	fi
}

function copy_config_cfg() {
	if [ -f $FLASH_CONFIG_FILE ]; then
		echo -n "create config.cfg..."
		cp -a $FLASH_CONFIG_FILE $IMAGE_PATH/config.cfg
		echo "done."
	else
		echo "$FLASH_CONFIG_FILE not fount!"
	fi
}

function copy_rk_parameter() {
	local HRR_INFO=""
	if [ "$BOARD_USES_AB_IMAGE" = true ]; then
		PARAMETER=${TARGET_DEVICE_DIR}/parameter_ab.txt
	elif [ "$HIGH_RELIABLE_RECOVERY_OTA" = "true" ]; then
		PARAMETER=${TARGET_DEVICE_DIR}/parameter_hrr.txt
		HRR_INFO=" (HIGH_RELIABLE_RECOVERY_OTA=$HIGH_RELIABLE_RECOVERY_OTA)"
	else
		PARAMETER=${TARGET_DEVICE_DIR}/parameter.txt
	fi
	if [ -f $PARAMETER ]; then
		echo -n "create parameter${HRR_INFO}..."
		cp -a $PARAMETER $IMAGE_PATH/parameter.txt
		echo "done."
	else
		echo "$PARAMETER not fount!"
	fi
}

function copy_base_parameter_img() {
	local TARGET_BASE_PARAMETER_IMAGE=`get_build_var TARGET_BASE_PARAMETER_IMAGE`
	if [ "$TARGET_BASE_PARAMETER_IMAGE"x != ""x ]; then
		if [ -f $TARGET_BASE_PARAMETER_IMAGE ]; then
			echo "create baseparameter..."
			cp -av $TARGET_BASE_PARAMETER_IMAGE $IMAGE_PATH/baseparameter.img
			echo "done."
		else
			echo "$TARGET_BASE_PARAMETER_IMAGE not fount!"
		fi
	fi
}

function build_ota_image() {
	if [ $TARGET == $BOOT_OTA ]; then
		if [ "$PRODUCT_USE_DYNAMIC_PARTITIONS" = "true" ]; then
			echo "Generate mass production super.img firmware that matches OTA ..."
			make dist -j32
			echo "re-generate super.img for mass production done "
			cp -rf $OUT/obj/PACKAGING/super.img_intermediates/super.img $IMAGE_PATH/
		fi
		echo -n "create system.img boot.img oem.img vendor.img dtbo.img vbmeta.img for OTA..."
		cp -rf $OUT/obj/PACKAGING/target_files_intermediates/*-target_files*/IMAGES/*.img $IMAGE_PATH/
		rm -rf $IMAGE_PATH/cache.img
		rm -rf $IMAGE_PATH/recovery-two-step.img
		if [ "$PRODUCT_USE_DYNAMIC_PARTITIONS" = "true" ]; then
			rm -rf $IMAGE_PATH/super_empty.img
		fi
		echo "done."
	fi
}

# ----------------------------------------------------------

echo IMAGE_PATH=$IMAGE_PATH
rm -rf $IMAGE_PATH; mkdir -p $IMAGE_PATH

make_dtbo_img
make_boot_img
make_recovery_img
make_vbmeta_img

if [ "$PRODUCT_USE_DYNAMIC_PARTITIONS" = "true" ]; then
	copy_super_img
else
	make_system_img
	make_vendor_img
	make_odm_img
fi

copy_misc_img
copy_uboot_img
copy_trust_img
copy_uboot_ro_img
copy_rk_loader

if [ "$PRODUCT_USE_KERNEL_IMAGES" = "true" ]; then
	copy_resource_img
	copy_kernel_img
fi

copy_rk_parameter
copy_base_parameter_img

build_ota_image

chmod a+r -R $IMAGE_PATH/

