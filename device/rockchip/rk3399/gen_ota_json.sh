#!/bin/bash

ota_file=$1
ota_json=$2
url_base=$3

# adb shell getprop ro.product.releasetype
romtype="stable"

# adb shell getprop ro.product.version
version="10.0.1"

#----------------------------------------------------------

function generate_ota_json() {
	local time=$(stat -c "%Y" "$1")
	local size=$(stat -c "%s" "$1")
	local zip_name=$(basename "$1")
	local id=$(echo "$zip_name" | sha1sum | cut -d' ' -f1)
	local build_id=$(cat build_number.txt)

	cat > $2 <<- EOF
	{
	  "response": [
	    {
	      "datetime": $time,
	      "filename": "$zip_name",
	      "id": "$id",
	      "romtype": "$romtype",
	      "size": $size,
	      "url": "$url_base/$zip_name",
	      "version": "$version",
	      "buildid": "$build_id"
	    }
	  ]
	}
	EOF
}

#----------------------------------------------------------

true ${ota_json:="$ota_file.json"}
true ${url_base:="http://112.124.9.243/ota/rk3399"}

echo "OTA file: $ota_file"
echo "OTA json: $ota_json"

if [ "$ota_file" = "empty" ]; then
	cat > $ota_json <<- EOF
	{
	  "response": [ ]
	}
	EOF

elif [ ! -f "$ota_file" ]; then
	echo "Error: '$ota_file': No such file"
	exit 1

else
	generate_ota_json $ota_file $ota_json
fi

echo "-----------------------------------------------------"
cat $ota_json

